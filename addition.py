def add(a, b):
    """Compute the sum of two numbers.

    Usage examples:
    >>> add(4.0, 2.0)
    6.0
    >>> add(1, 3)
    4
    """
    return a + b

print(add(1,2))